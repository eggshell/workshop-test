# Serverless Workshop

Welcome to the GitLab + Triggermesh serverless workshop!

Congratulations on getting logged in to you pre-provisioned account on the [lab instance](https://gitlab.tanuki.host)!

## Getting Help

During the workshop, you can join this [Gitter Room](https://gitter.im/serverless-workshop-velocity-2019/community) in order to get help from others in the workshop and from the authors themselves!

## Learning Objectives

1. Understand serverless computing architecture and use cases.
1. Understand how GitLab and Triggermesh can help you to develop and deliver
   serverless functions and applications.
1. Deploy a serverless function.
1. Deploy a serverless application.
1. See how Knative can be used to provide a happy path to a multicloud architecture.
1. Bonus: Set up GitLab as a Knative eventing source and use it to trigger functions
   on GitLab project events.

## Before We Begin

We will be doing pretty much all of our work in the GitLab UI, including viewing
and editing files. All you need is a laptop with a web browser installed.

You will notice that your account has exactly one project in it named
workshop. This is the project that we will work from for the duration of
these exercises, and also contains the instructions for each of the labs.

**NOTE:** It is **highly** recommended to keep one tab/window open with the instructions, and another
tab/window open to work from.

## Getting Started

These labs are self-paced. We recommend that you do them in the order that they have been written. When you finish a lab feel free to move to the next one. Lab 3 we will run as a demonstration.

Please proceed ahead to [lab 1](lab1/README.md).

## Labs

* [Lab 1 - Deploy a serverless function](./lab1/README.md)
* [Lab 2- Deploy a serverless application](./lab2/README.md)
* [Lab 3 - GitLab multi-cloud serverless](./lab3/README.md)
* [Lab 4 (bonus) - GitLab and Knative eventing](./lab4/README.md)