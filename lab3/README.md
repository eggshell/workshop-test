# Lab 3: Deploying onto Your Own Knative Cluster

While Serverless is about transparent infrastructure so that you can focus on writing code. You may deploy Knative on your own Kubernetes cluster.

GitLab has developed an integration so that you can do both: Create a Kubernetes cluster and install Knative on it.

Once you have done this you will be able to deploy your functions on your own cluster.

In this lab you will:

* Create a Kubernetes cluster
* Deploy Knative
* Configure a DNS domain to reach your functions
* Create a `gitlab-ci.yml` to deploy your functions

Let's get started:

## Get a Kubernetes Cluster

In the interest of time, we are going to show you all the steps live. You can watch us or follow along.

Please refer to the [official documentation](https://docs.gitlab.com/ce/user/project/clusters/)
on connecting GitLab with a Kubernetes cluster to learn how to attach a cluster
to your project.

If you use GKE, GitLab offers and integration to provision and attach a cluster
to a project for you. If you use another provider, you will still be able to
connect your cluster manually.

## Install Knative on Kubernetes Cluster

Once you have your cluster provisioned and attached to your project, hover
over the **Operations** tab in the left-hand sidebar, then click **Kubernetes**.

![Kubernetes Tab](./images/kubernetes-tab.png)

Next, click on your cluster. This will bring you to the GitLab Kubernetes
integration page. Scroll down to the **Applications** section and click the
**Install** button to the right of **Helm Tiller**.

![Install Helm Tiller](./images/install-helm-tiller.png)

This will install the server-side component of **Helm**, known as **Tiller** to
your cluster. Once it is installed, the you will be allowed to install the other
cloud-native applications via similar one-click installs.

Scroll down to the **Knative** section of the **Applications** list and enter
the domain name you will use. This requires access to some sort of Cloud DNS,
any provider will work, but you will need to own the domain name and it will
need to actually resolve once we get an IP.

Enter your FQDN in the **Knative Domain Name** field and click the **Install**
button. After the installation completes, you will have a Knative installation,
and an IP Address for your **Knative Endpoint** will generate. This can take
some time, but onces it completes, copy the IP Address in the **Knative Endpont**
text field and continue to the next section.

## Configure Knative domain

You will need to configure two DNS records, both of them **A** records and both
of them pointed to the same IP that serves as your **Knative Endpoint**:

1. `kdemo.example.com`
1. `*.kdemo.example.com`

The second is a wildcard DNS record, which will route all subdomains of
`kdemo.example.com` to your knative endpoint. This is needed as your serverless
functions and applications deployed with Knative will be assigned URLs as
subdomains of `kdemo.example.com`.

Once you configure the Knative installation properly you will see a similar window:

![](./images/knative-dns.png)

## Edit CI/CD Configuration

Finally, we will a new GitLab CI/CD configuration. You can start from what you already have, but to keep things as minimal as possible we will show it to you from scratch.

Your `.gitlab-ci.yaml` configuration will look like this:

```yaml
stages:
  - deploy-hello

################################################################################
#                                                                              #
#                                  Knative Jobs                                #
#                                                                              #
################################################################################

deploy-hello:
  stage: deploy
  image: gcr.io/triggermesh/tm:latest
  environment:
    name: production
  script:
    - tm -n "$KUBE_NAMESPACE" deploy service hello --from-image gcr.io/cloudrun/hello --wait; echo
```

Set the commit message to something like `Update .gitlab-ci.yaml`, then click
the green **Commit changes** button.

## Trigger the Pipeline

Hover over the **CI/CD** tab in the left-hand sidebar and click **Pipelines**.

Click the green **Run Pipeline** button. This will bring you to the **Run Pipeline**
screen, where you can define new environment variables to be used in specific
GitLab CI runs.

## GitLab Serverless Page

Hover over the **Operations** section in the left-hand sidebar and click on
**Serverless**. You will see that your **hello** function. This provides an easy way
to see what serverless functions and applications are deployed from your
project into your cluster, and provide easy ways to interact with them.

Try clicking on the button next to sample app, which will open the serverless
application in a new tab! You can also click on the function or application to
check how many pods are in use. If you install Prometheus into your cluster,
you will get function invocation metrics!

## Summary

Serverless computing is still a new frontier for the cloud native space. Today,
you have seen how it is possible to deploy serverless functions and applications
to multiple cloud environments with just a few configuration tweaks. Your application
code stays entirely untouched, allowing for maximum portability between managed
and self-hosted environments.

You have also seen how GitLab Serverless integrates into Knative, and our vision
for where we are going with serverless computing.
